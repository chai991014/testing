package models

import play.api.libs.json.{Json, Writes}

case class Product(
                     id:Option[Long],
                     productName: String,
                     cost: BigDecimal,
                     sellingPrice: BigDecimal,
                     profit: BigDecimal
                   )

object Product {
  implicit val writes: Writes[Product] = Json.writes[Product]
}
