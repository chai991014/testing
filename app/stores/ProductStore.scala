package stores

import java.sql.Connection
import anorm._
import anorm.SqlParser.scalar

import models.Product

class ProductStore {

  val parser: RowParser[Product] = Macro.namedParser[Product]

  def countAll(implicit conn: Connection): Long = {

    SQL("select count(*) as count from products").as(scalar[Long].single)
  }

  def countFiltered(searchText: String)(implicit conn: Connection): Long = {
    val searchCriteria =
      if(searchText.isEmpty)
        ""
      else
        "where (productName like {searchText})"

    SQL("select count(*) as count from products " + searchCriteria).on(
      "searchText" -> ("%" + searchText + "%")
    ).as(scalar[Long].single)
  }

  def search(start: Long, count: Long, searchText: String)(implicit conn: Connection): Seq[Product] = {
    val searchCriteria =
      if(searchText.isEmpty)
        ""
      else
        "where (productName like {searchText})"
    SQL("select * from products " + searchCriteria + "").on(
      "start" -> start,
      "count" -> count,
      "searchText" -> ("%" + searchText + "%")
    ).as(parser.*)
  }


  def findById(id: Long)(implicit conn: Connection): Option[Product] =
    SQL("select * from products where id = {id}").on(
      "id" -> id,
    ).as(parser.singleOpt)

  def findByProductName(productName: String)(implicit conn: Connection): Option[Product] =
    SQL("select * from products where productName = {productName}").on(
      "productName" -> productName,
    ).as(parser.singleOpt)

  def insert(product:Product )(implicit conn: Connection): Long = {
    SQL("insert into products (productName,cost,sellingPrice,profit) " +
      "values ({productName},{cost},{sellingPrice},{profit})").on(
      "productName" -> product.productName,
      "cost" -> product.cost,
      "sellingPrice"-> product.sellingPrice,
      "profit"-> product.profit,
    ).executeInsert().get
  }
  def update(product:Product )(implicit conn: Connection): Long = {
    SQL("update products set productName = {productName}, cost = {cost}, sellingPrice = {sellingPrice}, profit = {profit} where id = {id}").on(
      "productName" -> product.productName,
      "cost" -> product.cost,
      "sellingPrice" -> product.sellingPrice,
      "profit" -> product.profit,
      "id" -> product.id,
    ).executeUpdate()
  }

  def delete(id: Long)(implicit conn: Connection) =
    SQL("delete from products where id = {id}").on(
      "id" -> id
    ).executeUpdate()
}
