package controllers

import javax.inject._
import play.api._
import models._
import stores._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._
import play.api.db.Database
import play.api.libs.json.Json
import play.api.i18n.I18nSupport


class Products @Inject()(val controllerComponents: ControllerComponents,
                         db:Database,
                         productStore: ProductStore,
                       ) extends BaseController with I18nSupport  {
  val ProductForm: Form[Product] = Form (
    mapping(
      "id"->optional(longNumber),
      "productName" -> nonEmptyText,
      "cost" ->bigDecimal,
      "sellingPrice" -> bigDecimal,
      "profit" -> bigDecimal
    )(Product.apply)(Product.unapply)
  )

  def listProductJson = Action { implicit request =>
    val start = request.getQueryString("start").map(_.toLong).getOrElse(0L)
    val length = request.getQueryString("length").map(_.toLong).getOrElse(10L)
    val draw: Int = request.getQueryString("draw").map(_.toInt).getOrElse(0)
    val searchText = request.getQueryString("search[value]").getOrElse("")

    db.withConnection { implicit conn =>
      val total = productStore.countAll
      val filtered = productStore.countFiltered(searchText)
      val clockIn = productStore.search(start, length, searchText)
      println(clockIn)
      Ok(Json.obj(
        "draw" -> draw,
        "recordsTotal" -> total,
        "recordsFiltered" -> filtered,
        "data" -> clockIn
      ))
    }
  }

  def create = Action { implicit request =>
    db.withConnection{ implicit conn =>
      val (form, errors) =
        request.flash.get("errors") match {
          case Some(errorsStr) =>
            (ProductForm.bind(request.flash.data), errorsStr.split(","))
          case None =>
            (ProductForm.fill(Product(None, "", 0, 0, 0)), Array.empty[String])
        }
      Ok(views.html.products.form(form, errors, "Create"))
    }
  }

  def update(id: Long) = Action { implicit request =>
    db.withConnection{ implicit conn =>
      productStore.findById(id).map { product =>
        val (form, errors) =
          request.flash.get("errors") match {
            case Some(errorsStr) =>
              (ProductForm.bind(request.flash.data), errorsStr.split(","))
            case None =>
              (ProductForm.fill(Product(product.id, product.productName,product.cost,product.sellingPrice,product.profit)), Array.empty[String])
          }
        Ok(views.html.products.form(form, errors, "Update"))
      }.getOrElse(NotFound)
    }
  }

  def delete(id: Long) = Action { implicit request =>
    db.withConnection { implicit conn =>
      productStore.findById(id).map { product =>
        db.withTransaction { implicit conn =>
          try {
            productStore.delete(id)
            Redirect(routes.HomeController.index())
          } catch {
            case t: Throwable =>
              Redirect(routes.HomeController.index())
          }
        }
      }.getOrElse(NotFound)
    }
  }


  def postProductData = Action { implicit request =>
    ProductForm.bindFromRequest().fold(
      hasErrors = { form =>
        val id  = form.data.get("id").getOrElse("-1")
        if (id == ""){
          Redirect(routes.Products.create)
            .flashing(Flash(form.data)+
              ("errors" -> form.errors.map(_.key).mkString(",")))
        }
        else{
          Redirect(routes.Products.update(id.toLong))
            .flashing(Flash(form.data)+
              ("errors" -> form.errors.map(_.key).mkString(",")))
        }
      },
      success = { data =>
        db.withTransaction{ implicit conn =>
          productStore.findById(data.id.getOrElse(-1)) match {
            case Some(product)=>
              if(product.productName == data.productName){
                if(data.sellingPrice < 0 || data.cost < 0){
                  println("1111111111111111111111")
                  Redirect(routes.Products.update(product.id.get))
                    .flashing(Flash(ProductForm.fill(data).data) +
                      ("errors" -> "priceNotValid"))
                } else {
                  productStore.update(data)
                  Redirect(routes.HomeController.index())
                }
              }else{
                productStore.findByProductName(data.productName) match {
                  case Some(o) =>
                    Redirect(routes.Products.update(product.id.get))
                      .flashing(Flash(ProductForm.fill(data).data) +
                        ("errors" -> "productIsAlreadyExists"))
                  case None =>
                    println("2222222222222222")
                    if(data.sellingPrice < 0 || data.cost < 0){
                      Redirect(routes.Products.update(product.id.get))
                        .flashing(Flash(ProductForm.fill(data).data) +
                          ("errors" -> "priceNotValid"))
                    } else{
                      productStore.update(data)
                      Redirect(routes.HomeController.index())
                    }
                }
              }
            case None =>
              if(productStore.findByProductName(data.productName).isDefined){
                Redirect(routes.Products.create)
                  .flashing(Flash(ProductForm.fill(data).data) +
                    ("errors" -> "productIsAlreadyExists"))
              }else if(data.cost < 0 || data.sellingPrice <0 ){
                Redirect(routes.Products.create)
                  .flashing(Flash(ProductForm.fill(data).data) +
                    ("errors" -> "priceNotValid"))
              } else{
                val id: Long = productStore.insert(Product(None,data.productName, data.cost, data.sellingPrice, data.profit))
                Redirect(routes.HomeController.index())
              }
          }
        }
      }
    )
  }

}
