name := """hello-world"""
organization := "FUN"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.7"

libraryDependencies ++= Seq(
  guice,
  jdbc,
  ehcache,
  ws,
  evolutions,
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test,
  "mysql" % "mysql-connector-java" % "5.1.28",
  "org.playframework.anorm" %% "anorm" % "2.6.10",
)

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "FUN.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "FUN.binders._"
