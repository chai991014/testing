# --- !Ups

create table `products` (
  `id` bigint(20) not null auto_increment,
  `productName` varchar(100) not null,
  `cost` decimal(4,2) not null,
  `sellingPrice` decimal(5,2) not null,
  `profit` decimal(5,2) not null,
  primary key (`id`)
);

# --- !Downs

drop table `products`;